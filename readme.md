## Veeam vacancies counter

This application checks if actual amount of vacancies on https://careers.veeam.com/ match the expected number.

Run console application with the following parameters:

- Country name (string).
- Required language (string).
- Expected number of vacancies (integer).

For example, if you want to know if there shown 55 vacancies in United States with English as required language, use the following command: 
``` c
JobCounter.exe "United States" "English" 55
```