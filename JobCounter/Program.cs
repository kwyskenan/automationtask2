﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;

namespace JobCounter
{
    class Program
    {
        static void Main(string[] args)
        {
                       
            JobCounter jobs = new JobCounter();

            if (args == null || args.Length != 3 || !int.TryParse(args[2], out int expectedCount))
            {
                Console.WriteLine("Shitty arguments! Try again.");
                return;
            }

            if (jobs.ImplementSctipt(args[0], args[1]) == expectedCount)
            {
                Console.WriteLine("Good Job! Expected amount of vacancies match the actual, " + expectedCount.ToString() + " - is correct number");
            }
            else
            {
                Console.WriteLine("Expected value doesn't match the actual! Try again.");
            }
                        
            Console.ReadLine();

        }
    }
}
