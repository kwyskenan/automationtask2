﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;

namespace JobCounter
{
    class JobCounter
    {

        public int ImplementSctipt(string country, string language)
        {
            using (IWebDriver driver = new FirefoxDriver())
            {
                driver.Manage().Window.Maximize();
                driver.Navigate().GoToUrl("https://careers.veeam.com/");

                IWebElement countryElement = driver.FindElement(By.XPath("//*[@id='country-element']//span[@class='selecter-selected']"));
                IWebElement countryScroller = driver.FindElement(By.XPath("//*[@id='country-element']//*[@class='scroller-content']"));
                IWebElement certainCountry = driver.FindElement(By.XPath(string.Format("//*[@id='country-element']//span[@data-value='{0}']", country)));
                IWebElement languageElement = driver.FindElement(By.XPath("//*[@id='language']//span[@class='selecter-selected']"));
                IWebElement certainLanguage = driver.FindElement(By.XPath(string.Format("//*[@id='language']//label[normalize-space()='{0}']", language)));
                IWebElement applyLanguage = driver.FindElement(By.XPath("//a[@class='selecter-fieldset-submit']"));
                IWebElement showAllJobsButton = driver.FindElement(By.XPath("//*[@id='index-vacancies-buttons']//a[contains(text(), 'Show all jobs')]"));

                countryElement.Click();
                ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollTop = '500';", countryScroller);
                certainCountry.Click();
                languageElement.Click();
                certainLanguage.Click();
                applyLanguage.Click();

                if (showAllJobsButton.Displayed == true )
                {
                    showAllJobsButton.Click();
                }

                ReadOnlyCollection<IWebElement> vacancyBlocks = driver.FindElements(By.XPath("//*[contains(@class, 'vacancies-blocks-col')]"));
                int previousCount = 0;
                int currentCount = vacancyBlocks.Count;

                while (currentCount > previousCount)
                {
                    System.Threading.Thread.Sleep(500);
                    previousCount = currentCount;
                    vacancyBlocks = driver.FindElements(By.XPath("//*[contains(@class, 'vacancies-blocks-col')]"));
                    currentCount = vacancyBlocks.Count;
                }

                return currentCount;
            }
        }
    }
}

